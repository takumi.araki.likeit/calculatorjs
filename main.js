// main.js for CalculatorJs
// まずはHTMLとJavaScriptを連結するのに基本的なことをやっていかないといけない
// CSSでボタンを作る方法なんかもできるようにしたい

// document.addEventListener('keydown', (e) => なら、ボタンを出力できなくても
// 計算結果を処理することができるはず
// 演算子、の計算の処理と、結果を出力する処理を分ける必要はあるが、やることは同じだろう
// とりあえず、ロジックが組めれば良いのだから、できるやりかたでやってみるかな
// そのロジックを使って、HTMLとCSSに修正を加えていけばいい訳だし。

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

//screen size
const SCREEN_W = 360;
const SCREEN_H = 640;

// canvas size (canvasにコピーされた時に初めて表示される)
const CANVAS_W = SCREEN_W*2;
const CANVAS_H = SCREEN_H*2;

// field size
const FIELD_W = SCREEN_W*2;
const FIELD_H = SCREEN_H*2;

// creating canvas
let can = document.getElementById("can");
let con = can.getContext("2d");
can.width = CANVAS_W;
can.height = CANVAS_H; 

// とりあえず、フィールドにあるものを全て出力する
// creating field (virtual display)
//let vcan = document.createElement("canvas");
//let vcon = vcan.getContext("2d");
//vcan.width = FIELD_W;
//vcan.height = FIELD_H; 

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 初期化する変数
const GAMESPEED = 1000/9;
let tempVal = []; // 現在、入力されている数字を配列として記録
let tempValConv = 0; // tempValを数値としてかくのうする変数

let storeVal = 0; // 今まで、入力・計算された内容を記録, 最終的にはANSWERにも使用
let reloadOperand = 0; // 演算子入力のリロード間隔の変数
let reloadNum = 0; // 数字入力のリロード間隔の変数
// operand: 最後に入力された演算子を記憶するboolean型の配列
let operand = [false,false,false,false,false]; //0:addition,1:subtraction,2:multiplication,3:division,4:equal
// isAnswerDisplayed: ANSWERが出力された状態かどうか判別するboolean
let isAnswerDisplayed = false; // operand[4]としても良いが、分かりやすく新たにbooleanを定義
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 

// store keyboard status as an array
let keyCondition = [];

document.addEventListener('keydown', (e) =>
{
    console.log(e.code);
    keyCondition[e.code] = true;
});
document.addEventListener('keyup', (e) =>
{
    //console.log(e.code);
    keyCondition[e.code] = false;
});

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 演算子入力の処理

// 演算子処理のメイン, main
function operator () {

    // deleteの処理
    if(keyCondition['Backspace']&&reloadOperand===0) {
        deleteExecution();
    }

    // +,*,=の処理
    if (keyCondition['ShiftRight']) {
        if (keyCondition['Equal']&reloadOperand===0) {
            displayInput('+');
            operatorExecution(0); // 0:addition
        } else if (keyCondition['Digit8']&reloadOperand===0) {
            displayInput('*');
            operatorExecution(2); // 2:multiplication
        }
    } else if (keyCondition['Equal']&&reloadOperand===0) {
        displayInput('=');
        operatorExecution(4); // ４:equal
    }

    // -の処理
    if (keyCondition['Minus']&reloadOperand===0) {
        displayInput('-');
        operatorExecution(1); // 1:subtraction
    }

    // /の処理
    if (keyCondition['Slash']&reloadOperand===0) {
        displayInput('/');
        operatorExecution(3); //3:division
    }

    if (reloadOperand>0) reloadOperand--;
}

// Backspaceが押された時の処理
function deleteExecution () {
    console.log('Backspaceが押されました');

    if(isAnswerDisplayed) {
        displayInput('del');
        isAnswerDisplayed = false;
        return;
    }

    // 計算機を出力
    displayCalculator('del');

    // tempValの一番最後の要素を削除
    tempVal.splice(tempVal.length-1,1);
    // tempValの新しい値を出力
    let displayNum = convertTempVal(tempVal);
    displayInput(displayNum);
    // オペランド用のreload間隔をセット
    reloadOperand = 1;
}

// delete以外の演算子の処理
function operatorExecution (val) {
    // 配列を1つの数字に変換
    tempValConv = convertTempVal(tempVal);
    
    // オペランドが既に入力されているか確認し、されている場合、現在入力中の数値とそれまでの値を計算する
    checkOperand();

    // isAnswerDisplayedはcheckOperandの中でも使うため、checkOperandを呼んだ後に、falseにセットする
    // 演算子が押された場合、答えは出力されていないので、falseに
    isAnswerDisplayed = false;

    // コンソールに出力
    if (val===0) {
        console.log('+が押されました');
        displayCalculator('+');
        operand[val] = true;
    } else if (val===1) {
        console.log('-が押されました');
        displayCalculator('-');
        operand[val] = true;
    } else if (val===2) {
        console.log('*が押されました');
        displayCalculator('*');
        operand[val] = true;
    } else if (val===3) {
        console.log('/が押されました');
        displayCalculator('/');
        operand[val] = true;
    } else if (val===4) {
        console.log('=が押されました');
        displayCalculator('=');
        displayResult(storeVal);
        isAnswerDisplayed = true;
        operand[val] = true;
    }
    
    deleteTempVal();
    reloadOperand = 13;
}

// 演算子がtrueかfalseによって、演算を実行するメソッド
function checkOperand () {
    if (operand[0]) {
        // 演算子が連続で入力されていない場合（配列tempValの長さが0の場合）のみ、storeValの計算を行う
        if(tempVal.length!=0) {
            storeVal += tempValConv;
        }
        operand[0] = false;
    } else if (operand[1]) {
        if(tempVal.length!=0) {
            storeVal -= tempValConv;
        }
        operand[1] = false;
    } else if (operand[2]) {
        if(tempVal.length!=0) {
            storeVal *= tempValConv;
        }
        operand[2] = false;
    } else if (operand[3]) {
        if(tempVal.length!=0) {
            storeVal /= tempValConv;
        }
        operand[3] = false;
    } else if (operand[4]) {
        // 答えが既に出力されている場合は、直前の演算子がイコール(=)でもstoreValの値は更新しない
        if(!isAnswerDisplayed) {
            storeVal = tempValConv;
        }
        operand[4] = false;
    } else {
        console.log('no operand detected');

        // ? オペランドがセットされていなく、かつ、答えが出力されていない時だけ、storeValに現在の入力数値を代入
            
        // オペランドがセットされていない時は、storeValに現在入力されている数値を代入する
        storeVal = tempValConv; // = の後に、=だとtempValConvは必ず0

        console.log('storeVal', storeVal);
    }

    // 演算の処理を行なった後、一時的に入力された値は0にリセット
    tempValConv = 0;
}



// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 数字入力の処理

// 数値処理のメイン, main
function storeNumber () {
    if (keyCondition['Digit1']&&reloadNum===0) {
        storeNumberExecute(1);
    }
    if (keyCondition['Digit2']&&reloadNum===0) {
        storeNumberExecute(2);
    }
    if (keyCondition['Digit3']&&reloadNum===0) {
        storeNumberExecute(3);
    }
    if (keyCondition['Digit4']&&reloadNum===0) {
        storeNumberExecute(4);
    }
    if (keyCondition['Digit5']&&reloadNum===0) {
        storeNumberExecute(5);
    }
    if (keyCondition['Digit6']&&reloadNum===0) {
        storeNumberExecute(6);
    }
    if (keyCondition['Digit7']&&reloadNum===0) {
        storeNumberExecute(7);
    }
    if (!keyCondition['ShiftRight']&&keyCondition['Digit8']&&reloadNum===0) {
        storeNumberExecute(8);
    }
    if (keyCondition['Digit9']&&reloadNum===0) {
        storeNumberExecute(9);
    }
    if (keyCondition['Digit0']&&reloadNum===0) {
        storeNumberExecute(0);
    }

    // reloadの値が0でないときは、デクリメントで0になる様に処理
    if (reloadNum>0) reloadNum--;
}


// 数値が入力されたときの処理
function storeNumberExecute (val) {
    
    // 答えが出力されている場合、storeValの値を0にリセットし、isAnswerDisplayedはfalseに
    if (isAnswerDisplayed) {
        storeVal = 0;
        isAnswerDisplayed = false;
        console.log('storeNumberExecute, if(isAnswerDisplayed)');
    }    

    console.log(val+'が押されました');
    tempVal.push(val);

    // 数字を文字列に変換して、計算機のビジュアルを出力（===は完全一致なので文字列に変換する）
    val = String(val);
    displayCalculator(val);

    // 入力された値を出力する様の変数を定義、配列を変換
    let displayNum = convertTempVal(tempVal);
    displayInput(displayNum); // ここで入力された値(だけ)を出力するのではなく、配列での値を出力する
    reloadNum = 2;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 入力された数値の配列を変換・削除するブロック

// tempValをリセットするメソッド
function deleteTempVal () {
    for (let i=tempVal.length-1;i>=0;i--) {
        tempVal.splice(i,1);
    }
}
// 配列tempValの値を合計し、リターンするメソッド
function convertTempVal (obj) { // 引数をObjectにすれば、他の場所でも使える
    let result = 0;
    for (let i=obj.length-1, j=1;i>=0;i--) {
        result += obj[i] * j;
        j *= 10;
    }

    return result;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// display出力の処理

// 現在の入力情報を表示する関数
function displayInput (val) {
    console.log('inside displayInput');

    con.fillStyle = "black";
    con.fillRect(0,0,SCREEN_W,60); 

    if (val!=='del') {
        con.font = '40px serif'; //bold 48px serif 'font-related information such as font-size and font-style'
        con.fillStyle = "white";
        con.fillText(val, 40, 40); // num
    } else {
        con.fillStyle = "black";
        con.fillRect(0,0,SCREEN_W,60); 
    }

}

// 答えを表示する関数
function displayResult (val) {
    console.log('inside displayResult')

    // displayCalculatorで表示される計算機の上の部分を黒で塗りつぶし、リセット
    con.fillStyle = "black";
    con.fillRect(0,0,SCREEN_W,60);

    // 結果を出力
    con.font = '40px serif'; //bold 48px serif 'font-related information such as font-size and font-style'
    con.fillStyle = "white";
    con.fillText("ANSWER: " + val, 40, 40);

}

// 計算機全体のビジュアルを表示する関数
function displayCalculator (val) {
    con.fillStyle = "black";
    con.fillRect(60,60,SCREEN_W-60,SCREEN_H-60);


    // setting default values for display infomation
    let x = 20; // 初期, x座標
    let y = 60; // 初期, y座標
    let dx = 80; // 1マスの横幅
    let dy = 60; // 1マスの縦幅
    let fSize = 40; // デフォルトのフォントサイズ
    let spaceForDel = 0; // deleteを正しい位置に出力するためのスペース

    // creating the set of values to be displayed
    let displayArray = ['1','2','3','+','4','5','6','-','7','8','9','*','0','=','del','/'];


    let index = 0;
    // forLoopで計算機のマスを出力する (4x3)
    for (let i=0;i<4;i++) {
        for (let j=0;j<4;j++) {
            // fill the rectanble white to create a border
            con.fillStyle = "white";
            con.fillRect(x,y,dx,dy); //x座標,y座標,x幅,y幅
            // fill the rectanble black to fill the inside 
            // 現在入力中の数値または演算子のインデックスと一致する場合、白黒を反転させたい
            if (index === displayArray.indexOf(val)) {
                con.fillStyle = "white";
            } else {
                con.fillStyle = "black";
            }
            con.fillRect(x+1,y+1,dx-2,dy-2);
            // display the number from 1 to 9
            // delだけ3文字なのでfontsizeを30で定義
            if (index === displayArray.indexOf('del')) { // array.indexOf(String) = index of an array
                con.font = '30px serif';
                spaceForDel = 10;
            } else {
                con.font = '40px serif';
                spaceForDel = 0;
            }  

            if (index === displayArray.indexOf(val)) {
                con.fillStyle = "black";
            } else {
                con.fillStyle = "white";
            }
            con.fillText(displayArray[index],x+dx/2-fSize/5-spaceForDel,y+dy/2+fSize/3);
    
            x += dx;
            index++;
            //console.log('inside the for loop');
        }
        y += dy;
        x = 20;
    }

}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 全体の流れを作る処理、メイン,main

// initiating the game
function gameInit() {
    // 一番最初は計算機だけ出力する
    con.fillStyle = "black";
    con.fillRect(0,0,SCREEN_W,SCREEN_H);
    displayCalculator();
}

// start the game upon onloading
window.onload = function() {
    gameInit();
}
// * * * * * * * * * * * * * * * * * * * 
// gameLoopとsetInterval. 

// gameLoopの処理をGAMESPEEDの間隔で呼び続ける
setInterval(gameLoop, GAMESPEED);

// GAMESPEEDの間隔で常に呼び出し続ける関数
function gameLoop () {
    storeNumber();
    operator();

}